const { renderTemplateFile } = require("template-file");
import path from "path";
import { FileService, LogService } from "../services";
import Iterator from "./generator.iterator";
import {
  IDefinitions,
  IGeneratorArgs,
  IItem,
  IPropertiesMap,
  IProperty
} from "./generator.models";
import { capitalize, getSchemaName } from "./utils";

export default class InterfaceGenerator {
  public interfaces: { [name: string]: { [property: string]: string } } = {};
  private definitions: IDefinitions;
  private logService: LogService;
  private commonGenerator: Iterator;
  private fileService: FileService;
  private destinationFolderName: string;
  private fileName: string;
  private haveEnums: boolean;

  private tsTypes: { [k: string]: string } = {
    integer: "number"
  };

  constructor(args: IGeneratorArgs) {
    this.definitions = args.definitions;
    this.fileService = args.fileService;
    this.logService = args.logService;
    this.destinationFolderName = args.destinationFolderName;
    this.fileName = args.fileName;
    this.commonGenerator = new Iterator(args.definitions, this.writeInterface);
    this.haveEnums = false;
  }

  public generate = (haveEnums: boolean) => {
    this.haveEnums = haveEnums;
    this.commonGenerator.initiate();
    this.writeToFile();
  }

  private stringify = (interfaces: { [k: string]: any }): string => {
    let str = "";
    const keys = Object.keys(interfaces);
    keys.forEach((key, i) => {
      str += `  ${key}: ${interfaces[key]};`;
      if (i < keys.length - 1) {
        str += "\n";
      }
    });
    return str.slice(2);
  }

  private getRenderedTemplate = (
    tsInterface: { [property: string]: string },
    itr: string
  ): Promise<string> => {
    return renderTemplateFile(
      path.join(__dirname, "../", "templates", "interface.ts"),
      { interface_name: itr, interface_definitions: this.stringify(tsInterface) }
    );
  }

  private getPromises = (onDone: (str: string, index: number) => void) => {
    const keys = Object.keys(this.interfaces);
    const promises: Array<Promise<string>> = [];
    keys.forEach((itr, i) => {
      const tsInterface = this.interfaces[itr];
      const promise = this.getRenderedTemplate(tsInterface, itr)
        .then((renderedString: string) => { onDone(renderedString, i); return renderedString; });
      promises.push(promise);
    });
    return promises;
  }

  private writeToFile = () => {
    let str = "";
    const promises: Array<Promise<string>> = this.getPromises((renderedString, i) => {
      str += renderedString + "\n\n";
    });

    if (this.haveEnums) {
      str += `import * as Enums from './enums';\n\n`;
    }

    return Promise.all(promises)
      .finally(() => {
        this.fileService.writeToFile(this.fileName, str.slice(0, str.length - 1));
      })
      .then(() => {
        this.logService.log(`${this.destinationFolderName}/${this.fileName} was generated!`);
      })
      .catch((err: any) => { throw new Error(`Generator: generate interfaces file failed: ${err}`); });
  }

  private getParentName = (definitionName: string): string | undefined => {
    const definition = this.definitions[definitionName];

    if (!definition) {
      return;
    }

    const { allOf } = definition;
    if (allOf) {
      const model = allOf.find((a) => a.$ref);
      if (model && model.$ref) {
        return getSchemaName(model.$ref);
      }
    }
  }

  private isRequired = (defName: string, propName: string) => {
    const search = defName.split(" extends ")[0];
    const definition = this.definitions[search];

    if (definition && definition.required && definition.required.includes(propName)) {
      return true;
    }
  }

  private writeProperty = (defName: string, propName: string, type: string) => {
    if (!this.interfaces[defName]) {
      this.interfaces[defName] = {};
    }

    let name = propName;
    if (!this.isRequired(defName, propName)) {
      name += "?";
    }

    this.interfaces[defName][name] = type;
  }

  private writeProperties = (properties: IPropertiesMap, defName: string) => {
    Object.keys(properties).forEach((name: string) => {
      this.writeInterface(properties[name], defName, name);
    });
  }

  private getChainedModel = (chain: string, array: Array<{ $ref: string; }>, suffix = ""): string => {
    return array
      .map((a) => getSchemaName(a.$ref + suffix))
      .reduce((a, b) => `${a} ${chain} ${b}`);
  }

  private getItemsType = (items: IItem, propName: string, suffix = ""): string => {
    if (items.$ref) {
      return getSchemaName(items.$ref) + suffix;
    }

    if (items.oneOf) {
      if (suffix === "[]") {
        return `Array<${this.getChainedModel("|", items.oneOf)}>`;
      }

      return this.getChainedModel("|", items.oneOf, suffix);
    }

    if (items.anyOf) {
      return this.getChainedModel("&", items.anyOf, suffix);
    }

    if (items.items) {
      if (items.type === "array") {
        return `${items.items.type}[]`;
      }
      return items.items.type;
    }

    const itemType = this.tsTypes[items.type] || items.type;

    if (items.properties) {
      this.writeProperties(items.properties, capitalize(propName));
      return capitalize(propName) + suffix;
    }

    return itemType + suffix;
  }

  private writeArray = (property: IProperty, defName: string, propName: string) => {
    let type = "array";
    const { items } = property;

    if (items) {
      type = this.getItemsType(items, propName, "[]");
    }

    this.writeProperty(defName, propName, type);
  }

  private writeNoModelObject = (property: IProperty, propName: string) => {
    if (property.properties) {
      this.writeProperties(property.properties, capitalize(propName));
      Object.keys(property.properties).forEach((prop) => {
        this.writeInterface(property.properties[prop], capitalize(propName), prop);
      });
    }
  }

  private getObjectType = (property: IProperty, defName: string, propName: string) => {
    let type = "object";
    if (property.properties) {
      this.writeNoModelObject(property, propName);
      type = capitalize(propName);
    }

    if (property.additionalProperties) {
      const { items } = property.additionalProperties;
      const ref = property.additionalProperties.$ref;
      if (items && items.$ref) {
        type = `{ [k: string]: ${getSchemaName(items.$ref)}[] }`;
      } else if (ref) {
        type = `{ [k: string]: ${getSchemaName(ref)} }`;
      } else {
        type = `{ [k: string]: ${this.tsTypes[property.additionalProperties.type] || property.additionalProperties.type} }`;
      }
    }
    return type;
  }

  private writeObject = (property: IProperty, defName: string, propName: string) => {
    const type = this.getObjectType(property, defName, propName);

    if (property.allOf) {
      const properties = property.allOf.find((a) => a.properties);

      if (properties) {
        return this.writeNoModelObject(properties, defName);
      }
    }

    this.writeProperty(defName, propName, type);
  }

  private writeString = (property: IProperty, defName: string, propName: string) => {
    let type = "string";
    if (property.enum) {
      type = `Enums.${capitalize(propName)}`;
    }
    this.writeProperty(defName, propName, type);
  }

  private writeBoolean = (property: IProperty, defName: string, propName: string) => {
    this.writeProperty(defName, propName, "boolean");
  }

  private writeInteger = (property: IProperty, defName: string, propName: string) => {
    this.writeProperty(defName, propName, "number");
  }

  private writeInterface = (property: IProperty, defName: string, propName: string) => {
    interface ITypeToAction {
      [type: string]: (property: IProperty, defName: string, propName: string) => void;
    }

    const typeToAction: ITypeToAction = {
      array: this.writeArray,
      string: this.writeString,
      boolean: this.writeBoolean,
      integer: this.writeInteger,
      number: this.writeInteger,
      object: this.writeObject
    };

    const action = typeToAction[property.type];
    if (!action) {
      if (property.$ref) {
        const refWithParent = this.getDefinitionWithParent(defName);

        return this.writeProperty(refWithParent, propName, getSchemaName(property.$ref));
      }
      if (property.oneOf) {
        return this.writeProperty(defName, propName, this.getItemsType(property, propName));
      }
      this.logService.log(`@generator.interface: ${property} was not handled`);
    }

    const withParent = this.getDefinitionWithParent(defName);

    action(property, withParent || defName, propName);
  }

  private getDefinitionWithParent = (defName: string) => {
    const parent = this.getParentName(defName);

    if (parent) {
      return `${defName} extends ${parent}`;
    }

    return defName;
  }
}
