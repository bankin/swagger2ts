#!/usr/bin/env node
import axios, { AxiosRequestConfig } from "axios";
import { config } from "dotenv";
import fs from "fs";
config();

import Generator from "./generator";
import { FileService, LogService } from "./services";

const defaultFolderName = "swagger2Ts";

const {
  SWAGGER_FILE_PATH,
  SWAGGER_URL,
  SWAGGER_USERNAME,
  SWAGGER_PASSWORD,
  DESTINATION_FOLDER_NAME
} = process.env;

const argv = require("minimist")(process.argv.slice(2));
const argvPathToFile = argv.pathToFile;
const argvSwaggerUrl = argv.swaggerUrl;
const argvDestinationFolderName = argv.destinationFolderName;

const logService = new LogService();
const folderName = DESTINATION_FOLDER_NAME || argvDestinationFolderName || defaultFolderName;
const fileService = new FileService(folderName, logService);

const generate = (swagger: any) => {
  const generator = new Generator(
    swagger,
    folderName,
    fileService,
    logService
  );
  try {
    generator.generate();
  } catch (error) {
    throw new Error(`${SWAGGER_URL}: returned an unexpected response body\n${error}`);
  }
};

const swaggerUrl = SWAGGER_URL || argvSwaggerUrl;
const filePath = SWAGGER_FILE_PATH || argvPathToFile;

const generateFromFile = (path: string) => {
  fs.readFile(path, "utf8", (err: any, data: any) => {
    if (err) {
      throw new Error(err);
    }
    generate(JSON.parse(data));
  });
}

const generateFromUrl = (url: string) => {
  const options = { url } as AxiosRequestConfig;

  if (SWAGGER_PASSWORD && SWAGGER_PASSWORD) {
    options.auth = { username: SWAGGER_USERNAME as string, password: SWAGGER_PASSWORD };
  }

  axios(options).then((res) => generate(res.data));
}

if (filePath) {
  generateFromFile(filePath);
} else if (swaggerUrl) {
  generateFromUrl(swaggerUrl)
} else {
  logService.log(
    `
      ERROR:
      Some of the required environment files are missing: [SWAGGER_URL, SWAGGER_USERNAME, SWAGGER_PASSWORD].
      interfaces and enpoints files were NOT created!
    `
  );
}
