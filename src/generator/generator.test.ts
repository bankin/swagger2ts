// tslint:disable: max-classes-per-file

import {assert, expect} from "chai";
import path from "path";
import { spy } from "sinon";
import Generator from ".";
import { FileService, LogService } from "../services";

class MockLogService implements LogService {
  public log() {
    return null;
  }
}

const writeFileSpy = spy();
class MockFileService extends FileService {
  public writeToFile = (filename: string, content: string, templatePath?: string): Promise<string> => {
    return new Promise((r: any) => { writeFileSpy(filename, content, templatePath); r(); });
  }

  public readFile = (): Promise<string> => {
    return new Promise((r) => r(""));
  }

}

describe("Generator", () => {
  let generator: Generator;
  let destinationFolder: string;
  let mockFileService: FileService;
  const swagger = {
    definitions: {
      TestEndPoint: {
        type: "object",
        properties: {
          groupId: {
            type: "string",
            format: "uuid"
          },
          id: {
            type: "number",
            format: "int64"
          },
          userId: {
            type: "number",
            format: "int32"
          }
        }
      },
      TestInterface: {
        required: [
          "id",
          "userId"
        ],
        type: "object",
        allOf: [
          {
            $ref: "#/components/schemas/TestEndPoint"
          },
          {
            type: "object",
            properties: {
              id: {
                type: "number",
                format: "int64"
              },
              userId: {
                type: "number",
                format: "int32"
              }
            }
          }
        ]
      },
      TestComplexInterface: {
        type: "object",
        properties: {
          arrayOfInterfaces: {
            type: "array",
            items: {
              oneOf: [
                {
                  $ref: "#/components/schemas/TestInterface"
                },
                {
                  $ref: "#/components/schemas/TestEndPoint"
                }
              ]
            }
          },
          groupId: {
            type: "string",
            format: "uuid"
          },
          value: {
            type: "number",
            format: "int64"
          },
          name: {
            type: "number",
            format: "int32"
          }
        }
      },
      EnumTest: {
        type: "string",
        properties: {
          MyEnum: {
            type: "string",
            enum: ["test", "mock"]
          }

        }
      }
    },
    paths: {
      "/some/end-point/{context}": {
        get: {
          operationId: "testEndPoint",
          produces: ["*/*"],
          parameters: [
            {
              name: "context",
              in: "path",
              description: "context",
              required: true,
              type: "string"
            }
          ]
        }
      },
      "/some/enum/{deviceType}": {
        get: {
          operationId: "enumTest",
          parameters: [
            {
              name: "deviceType",
              in: "path",
              required: true,
              schema: {
                type: "string",
                enum: [
                  "ENERGY_CLUSTER",
                  "INVERTER_SINGLE_PHASE",
                  "OPTIMIZER",
                  "SMART_SOCKET",
                  "RELAY"
                ]
              }
            }
          ]
        }
      }
    }
  };

  before(() => {
    const mockLogService: LogService = new MockLogService();
    destinationFolder = "./dist";
    mockFileService = new MockFileService(destinationFolder, mockLogService);
    generator = new Generator(
      swagger,
      destinationFolder,
      mockFileService,
      mockLogService
    );
  });

  beforeEach(() => generator.generate());

  it("should set destination folder name", () => {
    expect(generator.destinationFolderName).to.be.equal(destinationFolder);
  });

  it("should write interfaces.ts file", (done) => {
    const content = `import * as Enums from './enums';

export interface TestEndPoint {
  groupId?: string;
  id?: number;
  userId?: number;
}

export interface TestInterface extends TestEndPoint {
  id: number;
  userId: number;
}

export interface TestComplexInterface {
  arrayOfInterfaces?: Array<TestInterface | TestEndPoint>;
  groupId?: string;
  value?: number;
  name?: number;
}

export interface EnumTest {
  MyEnum?: Enums.MyEnum;
}
`;

    setTimeout(() => {
      expect(writeFileSpy.calledWith("interfaces.ts", content)).to.be.true;
      done();
    }, 1000);
  });

  it("should write endpoints.ts file", () => {
    const content = `{
  testEndPoint: {
    method: 'get',
    getUrl: (context: string) => \`/some/end-point/\${context}\`
  },

  enumTest: {
    method: 'get',
    getUrl: (deviceType: string) => \`/some/enum/\${deviceType}\`
  }
}`;
    const templatePath = path.join(__dirname, "../", "templates", "export-object.ts");
    expect(writeFileSpy.calledWith("endpoints.ts", content, templatePath)).to.be.true;
  });

});
